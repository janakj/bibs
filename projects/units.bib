@InProceedings{allen04:units,
  author   = {Allen, Eric and Chase, David and Luchangco, Victor and Maessen,
    Jan-Willem and Steele Jr, Guy L},
  title	   = {Object-oriented units of measurement},
  booktitle= {ACM SIGPLAN Notices},
  year	   = 2004,
  volume   = 39,
  number   = 10,
  pages	   = {384-403},
  organization= {ACM},
  abstract = {Programs that manipulate physical quantities typically represent
    these quantities as raw numbers corresponding to the quantities'
    measurements in particular units (e.g., a length represented as a number
    of meters). This approach eliminates the possibility of catching errors
    resulting from adding or comparing quantities expressed in different units
    (as in the Mars Climate Orbiter error [11]), and does not support the safe
    comparison and addition of quantities of the same dimension. We show how
    to formulate dimensions and units as classes in a nominally typed
    object-oriented language through the use of statically typed metaclasses.
    Our formulation allows both parametric and inheritance poly-morphism with
    respect to both dimension and unit types. It also allows for integration
    of encapsulated measurement systems, dynamic conversion factors,
    declarations of scales (including nonlinear scales) with defined zeros,
    and nonconstant exponents on dimension types. We also show how to
    encapsulate most of the "magic machinery" that handles the algebraic
    nature of dimensions and units in a single meta-class that allows us to
    treat select static types as generators of a free abelian group.},
}

@Article{baldwin87:units,
  author   = {Baldwin, Geoff},
  title	   = {Implementation of Physical Units},
  journal  = {ACM SIGPLAN Notices},
  year	   = 1987,
  volume   = 22,
  number   = 8,
  pages	   = {45-50},
  publisher= {{ACM}},
  abstract = {This paper describes an implementation of strong typing with
    physical units. The typing system of a commercial Pascal compiler was
    minimally extended to allow compile-time checking of expressions involving
    physical units to ensure that the familiar semantics of dimensional
    analysis are not violated. The key concepts and mechanisms are described,
    as are the differences between ordinary type checking and that for
    physical units.},
}

@Article{dreiheller86:units,
  author   = {Dreiheller, A and Mohr, B and Moerschbacher, M},
  title	   = {Programming Pascal with Physical Units},
  journal  = {SIGPLAN Notices},
  year	   = 1986,
  volume   = 21,
  number   = 12,
  pages	   = {114-123},
  month	   = dec,
  issue_date= {Dec. 1986},
  issn	   = {0362-1340},
  numpages = 10,
  url	   = {http://doi.acm.org/10.1145/15042.15048},
  doi	   = {10.1145/15042.15048},
  acmid	   = 15048,
  publisher= {ACM},
  address  = {New York, NY, USA},
  abstract = {In /M&auml;86/ (SIGPLAN Notices 3/1986) M&auml;nner proposes an
    extension of Pascal permitting the use of physical units in programs. We
    discuss his issues in this paper and describe our own somewhat different
    approach. Our language extension PHYSCAL of Pascal not merely satisfies
    the requirements suggested by /M&auml;86/, but also supports predefined
    units (International Standard), thorough realisation of the concept of
    scale factors, input/output facilities for numbers with units. The new
    concepts are motivated, and the language description is given formally and
    by examples. Finally we discuss some details of the realised language
    implementation by a PHYSCAL-to-Pascal preprocessor in an UNIX
    environment.},
}

@Article{gehani85:ada,
  author   = {Gehani, Narain H.},
  title	   = {{Ada's Derived Types and Units of Measure}},
  journal  = {Software: Practice and Experience},
  year	   = 1985,
  volume   = 15,
  number   = 6,
  pages	   = {555-569},
  publisher= {Wiley Online Library},
  abstract = {Types in programming languages cannot model many properties of
    real world objects and quantities. Consequently, many errors resulting
    from the inconsistent usage of program objects representing real world
    objects and quantities cannot be detected automatically. For example, the
    real variables PRICE and WEIGHT, representing the price of diesel fuel and
    the weight of a person, may be inadvertently added, giving a non-sensical
    value; such an error cannot be detected by a compiler. The programming
    language Ada introduces the concept of derived types to tackle this
    problem. An alternative solution to this problem is the incorporation of
    units of measure as a new data attribute. Derived types only partially
    solve the problem of detecting the inconsistent usage of objects; some
    valid usages of objects are also not allowed. Moreover, the solution is
    inelegant and inconvenient to use. On the other hand, specification of
    units of measure solves the problem elegantly and conveniently. The two
    solutions are compared and analysed. Several ways to implement units of
    measure in Ada are examined.},
}

@TechReport{hamilton96:compact,
  author   = {Hamilton, Bruce},
  title	   = {A Compact Representation of Units},
  institution= {Measurement Systems Department, Hewlett-Packard Laboratories},
  year	   = 1996,
  number   = {HPL-96-61},
  abstract = {Units are useful in understanding the meaning of data about
    physical quantities. For example, “6 liters” gives more information than
    “6.” Other properties, such as accuracy and precision, also give useful
    information, but we do not discuss those here. Most units used in the
    physical sciences are well-standardized [1,2,3,22], and there are
    standards for the use and spelling of unit names [2,4], but there are few
    standards for the representation of units within computing systems. We
    present a representation which can accommodate unforeseen units, requires
    minimal agreement among the communicating parties, reveals the physical
    relationship among quantities, and requires only a few bytes, no matter
    how complex the unit.},
}

@Article{hayes95:units,
  author   = {Hayes, Ian J. and Mahony, Brendan P.},
  title	   = {Using units of measurement in formal specifications},
  journal  = {Formal Aspects of Computing},
  year	   = 1995,
  volume   = 7,
  number   = 3,
  pages	   = {329-347},
  issn	   = {0934-5043},
  doi	   = {10.1007/BF01211077},
  url	   = {http://dx.doi.org/10.1007/BF01211077},
  publisher= {Springer-Verlag},
  keywords = {Formal specification; Units of measurement; Dimensional
    analysis; Type-checking; Z specification language},
  language = {English},
}

@Article{hilfinger88:ada,
  author   = {Hilfinger, Paul N.},
  title	   = {{An Ada Package for Dimensional Analysis}},
  journal  = {ACM Transactions on Programming Languages and Systems},
  year	   = 1988,
  volume   = 10,
  number   = 2,
  pages	   = {189-203},
  month	   = apr,
  issue_date= {April 1988},
  issn	   = {0164-0925},
  numpages = 15,
  url	   = {http://doi.acm.org/10.1145/42190.42346},
  doi	   = {10.1145/42190.42346},
  acmid	   = 42346,
  publisher= {ACM},
  address  = {New York, NY, USA},
  abstract = {This paper illustrates the use of Ada's abstraction
    facilities—notably, operator overloading and type parameterization—to
    define an oft-requested feature: a way to attribute units of measure to
    variables and values. The definition given allows the programmer to
    specify units of measure for variables, constants, and parameters; checks
    uses of these entities for dimensional consistency; allows arithmetic
    between them, where legal; and provides scale conversions between
    commensurate units. It is not constrained to a particular system of
    measurement (such as the metric or English systems). Although the
    definition is in standard Ada and requires nothing special of the
    compiler, certain reasonable design choices in the compiler, discussed
    here at some length, can make its implementation particularly efficient.},
}

@Article{karr78:units,
  author   = {Karr, Michael and Loveman III, David B},
  title	   = {Incorporation of units into programming languages},
  journal  = {Communications of the ACM},
  year	   = 1978,
  volume   = 21,
  number   = 5,
  pages	   = {385-391},
  publisher= {ACM},
  abstract = {The issues of how a programming language might aid in keeping
    track of physical units (feet, sec, etc.) are discussed. A method is given
    for the introduction of relationships among units (a watt is volts*amps, a
    yard is three feet) and subsequent automatic conversion based upon these
    relationships. Various proposals for syntax are considered.},
}

@InCollection{kennedy94:dimension,
  author   = {Kennedy, Andrew},
  title	   = {Dimension Types},
  booktitle= {Programming Languages and Systems — ESOP '94},
  publisher= {Springer Berlin Heidelberg},
  year	   = 1994,
  editor   = {Sannella, Donald},
  volume   = 788,
  series   = {Lecture Notes in Computer Science},
  pages	   = {348-362},
  isbn	   = {978-3-540-57880-2},
  doi	   = {10.1007/3-540-57880-3_23},
  url	   = {http://dx.doi.org/10.1007/3-540-57880-3_23},
  abstract = {Scientists and engineers must ensure that physical equations are
    dimensionally consistent, but existing programming languages treat all
    numeric values as dimensionless. This paper extends a strongly-typed
    programming language with a notion of dimension type. Our approach
    improves on previous proposals in that dimension types may be polymorphic.
    Furthermore, any expression which is typable in the system has a most
    general type, and we describe an algorithm which infers this type
    automatically. The algorithm exploits equational unification over Abelian
    groups in addition to ordinary term unification. An implementation of the
    type system is described, extending the ML Kit compiler. Finally, we
    discuss the problem of obtaining a canonical form for principal types and
    sketch some more powerful systems which use dependent and higher-order
    polymorphic types.},
}

@Article{manner86:strong,
  author   = {M{\"a}nner, R},
  title	   = {Strong Typing and Physical Units},
  journal  = {ACM Sigplan Notices},
  year	   = 1986,
  volume   = 21,
  number   = 3,
  pages	   = {11-20},
  publisher= {ACM},
  abstract = {This paper proposes a syntactic extension of high level
    languages like PASCAL or ADA to allow the usage of physical quantities,
    i.e. numbers with units, instead of just numbers. It offers enhanced type
    checking for expressions and assignments during compilation time.
    Additionally, it allows problem oriented simple scaling of variables and
    local scale selection. Furthermore, it reduces the number of coding errors
    due to wrongly chosen scales. After a discussion of mutual relations
    between strong typing in programming languages and physical units,
    examples are shown to demonstrate the usefulness of the proposed
    extension. Finally a graph representation of the new syntactic elements is
    given.},
}

@Article{novak95:conversion,
  author   = {Novak, Gordon S},
  title	   = {Conversion of Units of Measurement},
  journal  = {IEEE Transactions on Software Engineering},
  year	   = 1995,
  volume   = 21,
  number   = 8,
  pages	   = {651-661},
  publisher= {IEEE},
  doi	   = {10.1109/32.403789},
  abstract = {Algorithms are presented for converting units of measurement
    from a given form to a desired form. The algorithms are fast, are able to
    convert any combination of units to any equivalent combination, and
    perform dimensional analysis to ensure that the conversion is legitimate.
    Algorithms are also presented for simplification of symbolic combinations
    of units. Application of these techniques to perform automatic unit
    conversion and unit checking in a programming language is described.},
}

@InProceedings{wand91:inference,
  author   = {Wand, Mitchell and O'Keefe, Patrick},
  title	   = {Automatic Dimensional Inference.},
  booktitle= {Computational Logic-Essays in Honor of Alan Robinson},
  year	   = 1991,
  pages	   = {479-483},
}


@TechReport{schadow09:ucum,
  author   = {Schadow, Gunther and McDonald, Clement J},
  title	   = {The Unified Code for Units of Measure},
  institution= {Regenstrief Institute and UCUM Organization},
  year	   = 2013,
  url	   = {http://unitsofmeasure.org/ucum.html},
}

@TechReport{thomson08:nist811,
  author   = {Ambler Thompson and Barry N. Taylor},
  title	   = {{NIST Special Publication 811: Guide for Use of International
    System of Units (SI)}},
  institution= {{National Institude for Standards and Technology (NIST)}},
  year	   = 2008,
  month	   = mar,
  url	   = {http://physics.nist.gov/cuu/pdf/sp811.pdf},
}


@techreport{siunits,
  title	   = {{The International System of Units (SI), 8th Edition}},
  author   = {},
  institution= {Bureau International des Poids et Mesures},
  year	   = 2006,
  url	   = {http://www.bipm.org/utils/common/pdf/si_brochure_8_en.pdf},
}


@TechReport{id:senml,
  author   = {C. Jennings and Z. Shelby and J. Arkko},
  title	   = {{Media Types for Sensor Markup Language (SENML)}},
  institution= {{IETF Secretariat}},
  year	   = 2012,
  type	   = {{Internet-Draft}},
  month	   = oct,
  howpublished= {\url{http://www.ietf.org/id/draft-jennings-senml-10.txt}},
  abstract = {This specification defines media types for representing simple
    sensor measurements and device parameters in the Sensor Markup Language
    (SenML). Representations are defined in JavaScript Object Notation (JSON),
    eXtensible Markup Language (XML) and Efficient XML Interchange (EXI),
    which share the common SenML data model. A simple sensor, such as a
    temperature sensor, could use this media type in protocols such as HTTP or
    CoAP to transport the measurements of the sensor or to be configured.},
}


@Misc{MIXF,
  author   = {{Aubrey Jaffer}},
  title	   = {{Representation of numerical values and SI units in character
    strings for information interchanges (MIXF)}},
  howpublished= {\url{http://people.csail.mit.edu/jaffer/MIXF}},
  note	   = {{A}ccessed: Apr. 2014},
}

@Misc{QUDT,
  title	   = {Quantities, Units, Dimensions, and Data Type Ontologies},
  howpublished= {\url{http://www.qudt.org/}},
  note	   = {{A}ccessed: Apr. 2014},
  authors  = {Ralph Hodgson and Paul J. Keller and Jack Hodges and Jack
    Spivak},
}

@Misc{JSON-LD,
  title	   = {A JSON-based Serialization for Linked Data},
  howpublished= {\url{http://www.w3.org/TR/json-ld/}},
  note	   = {{A}ccessed: Apr. 2014},
}

