
.PHONY: all
all: fetch convert

.PHONY: fetch
fetch:
	scripts/fetch

.PHONY: convert
convert:
	scripts/convert

